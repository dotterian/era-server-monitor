import React, {Component} from 'react';

class Button extends Component {
  render() {
    let text = '';
    if (this.props.icon) {
        text = (<span><i className={`fa fa-${this.props.icon}`}/> {this.props.text}</span>);
    } else {
        text = (<span>{this.props.text}</span>);
    }
    return <button type="button" className={`btn btn-sm btn-${this.props.type}`} onClick={this.props.click}>{text}</button>;
  }
}

export default Button;
