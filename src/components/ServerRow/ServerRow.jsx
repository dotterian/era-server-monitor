import React, { Component } from 'react';
import 'whatwg-fetch';
import Button from "../Button";

class ServerRow extends Component {
  constructor() {
    super();
    this.state = {
      active: false,
      delay: 1000
    };
    this.apiUrl = 'apirecords';
    this.statusMethod = 'getstatus';
    this.startTransactionsMethod = 'runtransactions';
    this.stopTransactionsMethod = 'stoptransaction';
    this.startForgingMethod = 'forgingstart';
    this.stopForgingMethod = 'forgingend';
    this.clearCountMethod = 'clear-transaction-count';
    this.timeout = 5; // Таймаут в секундах
  }

  getApiUrl(method) {
    return `http://${this.props.server}:${this.props.port}/${this.apiUrl}/${method}`;
  }

  getStatus = () => {
    const url = this.getApiUrl(this.statusMethod);
    fetch(url)
      .then(response => response.json())
      .then((result) => {
        this.setState({
          active: true,
          json: result,
        });
      })
      .catch((error) => {
        this.setState({
          active: false
        });
      });
  };

  startForging = () => {
    fetch(this.getApiUrl(this.startForgingMethod));
  };

  stopForging = () => {
    fetch(this.getApiUrl(this.stopForgingMethod));
  };

  clearCount = () => {
    fetch(this.getApiUrl(this.clearCountMethod));
  };

  sendTransaction = () => {
    fetch(this.getApiUrl(this.startTransactionsMethod) + `?sleep=${this.state.delay}`);
  };

  stopTransaction = () => {
    fetch(this.getApiUrl(this.stopTransactionsMethod));
  };

  componentDidMount() {
    this.getStatus();
    this.interval = setInterval(this.getStatus, this.timeout * 1000);
  }

  render() {
    const json = this.state.json;
    return (
      <tr className="server-row">
        <td>
          <a href={`http://${this.props.server}:${this.props.port}/index/blockexplorer.html`} target="_blank">{this.props.server}</a><br/>
          {this.state.active?json.network_name:'---'}
        </td>
        <td>
          {this.state.active?json.forging_name:'---'}&nbsp;
          <Button type="success" icon="play" click={this.startForging} />
          <Button type="danger" icon="pause" click={this.stopForging} /><br/>
          {this.state.active?json.block_generator_status:''}
        </td>
        <td>
          {this.state.active?`${json.block_timestamp} ${json.block_height}-${json.block_transaction_count}`:'---'}
          <br/>{this.state.active?`${json.block_vin_value} / ${json.block_value_target}`:''}
        </td>
        <td>{this.state.active?this.state.json.block_creator_address:'---'}</td>
        <td>{this.state.active?this.state.json.unconfirmed_transactions:'---'}</td>
        <td>
          {this.state.active?this.state.json.count:'---'}&nbsp;
          <Button type="danger" icon="refresh" click={this.clearCount} />
        </td>
        <td>
          {this.state.active?this.state.json.transaction_status:'---'}<br/>
          <input type="number" name="delay" value={this.state.delay} onChange={(e) => {this.setState({delay: e.target.value})}} />
          <Button type="success" icon="play" click={this.sendTransaction} />
          <Button type="danger" icon="pause" click={this.stopTransaction} />
        </td>
      </tr>
    );
  }
}

export default ServerRow;
