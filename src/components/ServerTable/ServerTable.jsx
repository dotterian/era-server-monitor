import React, { Component } from 'react';
import ServerRow from "../ServerRow/ServerRow";

class ServerTable extends Component {

  render() {
    let rows = [];
    this.props.config.servers.forEach((server) => {
      rows.push(<ServerRow server={server} port={this.props.config.port} />);
    });
    return (
      <table className="table">
        <thead>
          <tr>
            <th>IP<br/>Подключение к ERA</th>
            <th>Статус форджинга<br/>Статус генератора блоков</th>
            <th>Последний блок<br/>Дата Высота - Кол.транзакций<br/>Сила / Target</th>
            <th>Автор блока</th>
            <th>Неподтверждённые<br/>транзакции</th>
            <th>Отправленые<br/>транзакции</th>
            <th>Генератор транзакций (Задержка ms)</th>
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
    );
  }
}

export default ServerTable;
