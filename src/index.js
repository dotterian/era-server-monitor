import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

// eslint-disable-next-line no-undef
ReactDOM.render(<App config={config} />, document.getElementById('root'));
