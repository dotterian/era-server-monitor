import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ServerTable from "./components/ServerTable/ServerTable";

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Era server monitor</h1>
        </header>
        <ServerTable config={this.props.config}/>
      </div>
    );
  }
}

export default App;
